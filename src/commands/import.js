const { Command } = require('@oclif/command')
const Listr = require('listr')
const client = require('../utils/graphql-client')
const fetch = require('node-fetch')
const fs = require('fs-extra')
const path = require('path')
const notifier = require('node-notifier')

// const createDownloadAuth = `mutation {
//   createDownloadAuth
// }
// `

const tasks = new Listr([
  {
    title: 'Preparing Download',
    task: async ctx => {
      const { createDownloadAuth } = await client.request('mutation{createDownloadAuth}')
      ctx.download = createDownloadAuth
      return Promise.resolve()
    }
  },
  {
    title: 'Downloading File',
    task: async ({ download, bit }) => fetch(`${download.downloadUrl}/file/bitstor/${download.downloadAuthorization.fileNamePrefix}/${bit}`, {
      headers: { Authorization: download.downloadAuthorization.authorizationToken }
    })
    .then(res => {
      return new Promise((resolve, reject) => {
        const fileName = path.basename(bit)
        const dest = fs.createWriteStream(fileName)
        res.body.pipe(dest)
        res.body.on('error', err => {
          reject(err)
        })
        dest.on('finish', () => {
          resolve()
        })
        dest.on('error', err => {
          reject(err)
        })
      })
    })
  }
])

class ImportCommand extends Command {
  async run() {
    const { args } = this.parse(ImportCommand)
    try {
      await tasks.run(args)
      notifier.notify({
        title: 'Successful import!',
        message: `You have successfully imported ${args.bit}`,
        timeout: 3
      })
    } catch (error) {
      this.log(error)
    }
  }
}

ImportCommand.description = `Import a component
...
Import a component from a space into the current folder
`

ImportCommand.args = [
  {
    name: 'bit',
    required: true,
    description: 'Enter the Bit space and name'
  }
]

module.exports = ImportCommand
