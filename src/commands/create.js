const { Command } = require('@oclif/command')
const inquirer = require('inquirer')
const Listr = require('listr')
const client = require('../utils/graphql-client')
const notifier = require('node-notifier')

let spaceName
const prompt = inquirer.createPromptModule()
const createQuestions = [
  {
    type: 'input',
    name: 'name',
    message: 'Enter the name for your new Space',
    default: () => spaceName ? spaceName : null
  },
  {
    type: 'input',
    name: 'description',
    message: 'Enter a description (optional)'
  }
]

const createSpaceMutation = `mutation ($name: String!, $description: String) {
  createSpace(name: $name, description: $description) {
    name
  }
}
`

const tasks = new Listr([
  {
    title: 'Creating Space',
    task: async ctx => client.request(createSpaceMutation, ctx.space)
  }
])

class CreateCommand extends Command {
  run() {
    const { args: { name } } = this.parse(CreateCommand)
    spaceName = name
    prompt(createQuestions).then(async space => {
      try {
        await tasks.run({ space })
        notifier.notify({
          title: 'Created Space',
          message: `You have created a new Space called ${name ? name : space.name}`,
          timeout: 3
        })
      } catch (error) {
        this.log('Error creating Store. Please try again.')
      }
    })
  }
}

CreateCommand.description = `Create Space
...
Create a new Space for components
`

CreateCommand.args = [
  {
    name: 'name',
    required: false,
    description: 'Enter the Space name'
  }
]

module.exports = CreateCommand
