const { Command } = require('@oclif/command')
const fetch = require('node-fetch')
const client = require('../utils/graphql-client')

const { createReadStream, statSync } = require('fs-extra')
const path = require('path')
const mime = require('mime-types')

const conf = require('rc')('bitstor-')

const inquirer = require('inquirer')
const Listr = require('listr')
const notifier = require('node-notifier')

const getSignedUrlMutation = `mutation {
  createSignedUrl
}`
const getSpacesQuery = `{
  spaces {
    id
    name
  }
}`
const createBitMutation = `mutation ($name: String!, $file: String!, $version: Float!) {
  createBit (name: $name, file: $file, version: $version) {
    id
  }
}`

const prompt = inquirer.createPromptModule()
const addQuestions = [
  {
    type: 'list',
    name: 'space',
    message: 'What space do you want to add this component to?',
    choices: async () => {
      const { spaces } = await client.request(getSpacesQuery)
      return spaces
    }
  }
]

const tasks = new Listr([
  {
    title: 'Getting Upload URL',
    task: async ctx => {
      const { createSignedUrl } = await client.request(getSignedUrlMutation)
      ctx.signedUrl = createSignedUrl
      return Promise.resolve()
    }
  },
  {
    title: 'Uploading Component',
    task: async ctx => {
      const { uploadUrl, authorizationToken } = ctx.signedUrl
      const { space, file } = ctx
      const fileType = await mime.contentType(path.extname(file))
      const { size: fileSize } = await statSync(file)
      const stream = await createReadStream(file)
      return fetch(uploadUrl, {
        method: 'POST',
        body: stream,
        headers: {
          authorization: authorizationToken,
          'Content-Type': fileType,
          'Content-Length': fileSize,
          'X-Bz-File-Name': `${conf.user.id}/${space}/${file}`,
          'X-Bz-Content-Sha1': 'do_not_verify'
        }
      })
      .then(res => res.json())
      .then(json => {
        ctx.result = json
      })
      .catch(error => {
        ctx.result = error
      })
    }
  },
  {
    title: 'Registering Component',
    task: async ({ result, file }) => {
      const { name } = await path.parse(file)
      return client.request(createBitMutation, { name, file: result.fileName, version: result.uploadTimestamp })
    }
  }
])

class AddCommand extends Command {
  async run() {
    const { args: { file } } = this.parse(AddCommand)
    prompt(addQuestions).then(async ({ space }) => {
      try {
        await tasks.run({ space, file })
        notifier.notify({
          title: 'Added Bit',
          message: `Nice! You have just added ${file} to ${space}`,
          timeout: 3
        })
      } catch (error) {
        this.log('Error creating Store. Please try again.')
      }
    })
  }
}

AddCommand.description = `Add a component
...
Register a new component, and add it to a store
`

AddCommand.args = [
  {
    name: 'file',
    required: true,
    description: 'Type the path to the component you want to add'
  }
]

module.exports = AddCommand
