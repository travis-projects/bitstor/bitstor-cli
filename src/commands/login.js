const { Command } = require('@oclif/command')
const inquirer = require('inquirer')
const fs = require('fs-extra')
const Listr = require('listr')
const homedir = require('os').homedir()
const client = require('../utils/graphql-client')
const notifier = require('node-notifier')

const filepath = `${homedir}/.bitstor-rc`

const prompt = inquirer.createPromptModule()
const loginQuestions = [
  {
    type: 'input',
    name: 'email',
    message: 'Enter your email address'
  },
  {
    type: 'password',
    name: 'password',
    message: 'Enter your password'
  }
]

// Mutations
const loginMutation = `mutation login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    user {
      id
      email
    }
    token
  }
}`

// const signupMutation = `mutation signup($email: String!, $password: String!) {
//   signup(email: $email, password: $password) {
//     user {
//       id
//       email
//     }
//     token
//   }
// }`

const writeRc = config => fs.writeJson(filepath, config)

const tasks = new Listr([
  {
    title: 'Logging you in',
    task: async ctx => {
      const { login } = await client.request(loginMutation, ctx.user)
      ctx.configs = login
      return Promise.resolve()
    }
  },
  {
    title: 'Creating config file',
    task: async ctx => writeRc(ctx.configs)
  }
])

class LoginCommand extends Command {
  async run() {
    prompt(loginQuestions).then(async user => {
      try {
        await tasks.run({ user })
        notifier.notify({
          title: 'Successful Login',
          message: `You have successfully logged in as ${user.email}`,
          timeout: 3
        })
      } catch (error) {
        this.log('Error logging in...')
      }
    })
  }
}

LoginCommand.description = `Login to BitStor
...
Login to the BitStor service
`

module.exports = LoginCommand
