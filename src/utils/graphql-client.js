const { GraphQLClient } = require('graphql-request')
const conf = require('rc')('bitstor-', { endpoint: 'https://europe-west1-cirrus-creative.cloudfunctions.net/graphql' })

const client = new GraphQLClient(conf.endpoint, {
  headers: {
    Authorization: conf.token ? conf.token : null
  }
})

module.exports = client
