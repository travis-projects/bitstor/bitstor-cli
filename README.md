bitstor-cli
===========

Reuse your Vue components

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/bitstor-cli.svg)](https://npmjs.org/package/bitstor-cli)
[![Downloads/week](https://img.shields.io/npm/dw/bitstor-cli.svg)](https://npmjs.org/package/bitstor-cli)
[![License](https://img.shields.io/npm/l/bitstor-cli.svg)](https://github.com/thetre97/bitstor-cli/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g bitstor-cli
$ bitstor COMMAND
running command...
$ bitstor (-v|--version|version)
bitstor-cli/1.0.2 linux-x64 node-v11.0.0
$ bitstor --help [COMMAND]
USAGE
  $ bitstor COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`bitstor add FILE`](#bitstor-add-file)
* [`bitstor create [NAME]`](#bitstor-create-name)
* [`bitstor help [COMMAND]`](#bitstor-help-command)
* [`bitstor import BIT`](#bitstor-import-bit)
* [`bitstor login`](#bitstor-login)

## `bitstor add FILE`

Add a component

```
USAGE
  $ bitstor add FILE

ARGUMENTS
  FILE  Type the path to the component you want to add

DESCRIPTION
  ...
  Register a new component, and add it to a store
```

_See code: [src/commands/add.js](https://github.com/thetre97/bitstor-cli/blob/v1.0.2/src/commands/add.js)_

## `bitstor create [NAME]`

Create Space

```
USAGE
  $ bitstor create [NAME]

ARGUMENTS
  NAME  Enter the Space name

DESCRIPTION
  ...
  Create a new Space for components
```

_See code: [src/commands/create.js](https://github.com/thetre97/bitstor-cli/blob/v1.0.2/src/commands/create.js)_

## `bitstor help [COMMAND]`

display help for bitstor

```
USAGE
  $ bitstor help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.1.3/src/commands/help.ts)_

## `bitstor import BIT`

Import a component

```
USAGE
  $ bitstor import BIT

ARGUMENTS
  BIT  Enter the Bit space and name

DESCRIPTION
  ...
  Import a component from a space into the current folder
```

_See code: [src/commands/import.js](https://github.com/thetre97/bitstor-cli/blob/v1.0.2/src/commands/import.js)_

## `bitstor login`

Login to BitStor

```
USAGE
  $ bitstor login

DESCRIPTION
  ...
  Login to the BitStor service
```

_See code: [src/commands/login.js](https://github.com/thetre97/bitstor-cli/blob/v1.0.2/src/commands/login.js)_
<!-- commandsstop -->
